#!/bin/bash

echo "Récupération des données depuis firebase"
node index.js

echo "Copie des données récupérées dans hadoop"
hdfs dfs -copyFromLocal data-transaction.csv /tmp/data/
hdfs dfs -copyFromLocal data-position.csv /tmp/data/
hdfs dfs -copyFromLocal data-products.csv /tmp/data/
hdfs dfs -copyFromLocal data-profile.csv /tmp/data/

echo "Modification des droits des fichiers copiées"
hdfs dfs -chmod -R 777 /tmp/data

echo "Création des Tables si elle n'existe pas"
hive -f scriptfile-createTable.hql

echo "Insertion au format orc des données dans la table et suppression de la table temporaire"
hive -f insert-data.hql

echo "Suppression du fichier, pour ne pas surcharger notre stockage"
#hdfs dfs –rmr  /tmp/data/data-transaction.csv
#hdfs dfs –rmr  /tmp/data/data-position.csv
#hdfs dfs –rmr  /tmp/data/data-products.csv
#hdfs dfs –rmr  /tmp/data/data-profile.csv

echo "Suppresion du fichier data.csv en local"
#rm -rf data-transaction.csv
#rm -rf data-position.csv
#rm -rf data-products.csv
#rm -rf data-profile.csv

python recommendation.py
