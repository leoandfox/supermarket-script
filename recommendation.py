import json

the_document = []

with open('data-transaction.json') as f:
    transactions = json.load(f)
with open('data-transaction.json') as f:
    products = json.load(f)
with open('data-profile.json') as f:
    users = json.load(f)

recommendations = []
allRecommendations = []
recommendationForUser = []


def checkIfProductInArray(product, listProduct):
    for oneProduct in listProduct:
        if product["uid"] == oneProduct["uid"]:
            return True
    return False


def incrementValue(product):
    theIndex = next((i for i, item in enumerate(recommendations) if item["uid"] == product["uid"]), None)
    recommendations[theIndex]["numberOfItemInCart"] += product["numberOfItemInCart"]


def sort(theList):
    if len(theList) > 0:
        newlist = sorted(theList, key=lambda k: k['numberOfItemInCart'], reverse=True)
        newlist = newlist[0]
        return newlist


for user in users:
    recommendations = []
    for transaction in transactions:
        if transaction["userId"] == user["userId"]:
            for product in transaction['listProducts']:
                if not checkIfProductInArray(product, user["shoppingCart"]):
                    if len(recommendations) != 0:
                        if checkIfProductInArray(product, recommendations):
                            incrementValue(product)
                        else:
                            recommendations.append(product)
                    else:
                        recommendations.append(product)
    recommendationForUser = {"userId": user["userId"], "recommendation": sort(recommendations)}
    allRecommendations.append(recommendationForUser)
newTable = {'Recommendations': allRecommendations}

with open('recommendation.json', 'w') as f:
    json.dump(newTable, f)
