const admin = require('firebase-admin');
const fs = require('fs');
const readFile = require('fs').readFile;
const writeFile = require('fs').writeFile;
const fields = ['date', 'listProducts', 'userId', 'totalPrice'];


var serviceAccount = require("./supermarket-c24f6-6c2cf21808e2.json");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://supermarket-c24f6.firebaseio.com"
});

let db = admin.firestore();

db.collection('Recommendations').get()
  .then(snapshot => {
    snapshot.forEach(doc => {
      db.collection('Recommendations').doc(doc.id).delete();
    });
  })
  .catch(err => {
    console.log('Error deleting collection', err);
  });

db.collection('Transaction').get()
  .then((snapshot) => {
    var indexDoc = 0;
    var myList  = [];
    snapshot.forEach((doc) => {
      myList[indexDoc] = doc.data();
      indexDoc++;
    });
    let myJson = JSON.stringify(myList);
    writeFile('data-transaction.json', myJson, (err) => {
      if(err) {
          console.log(err); // Do something to handle the error or just throw it
          throw new Error(err);
      }
      console.log('Success!');
  });
    
  })
  .catch((err) => {
    console.log('Error getting documents', err);
  });
db.collection('HistoryPosition').get()
  .then((snapshot) => {
    var indexDoc = 0;
    var myList = [];
    snapshot.forEach((doc) => {
      myList[indexDoc] = doc.data();
      indexDoc++;
    });
    let myJson = JSON.stringify(myList);
    writeFile('data-position.json', myJson, (err) => {
      if(err) {
          console.log(err); // Do something to handle the error or just throw it
          throw new Error(err);
      }                                                                                                                                              
      console.log('Success!');
  });
                                                                                                                                                     
  })
  .catch((err) => {
    console.log('Error getting documents', err);
  });
      
db.collection('Products').get()
  .then((snapshot) => {
    var indexDoc = 0;
    var myList = [];
    snapshot.forEach((doc) => {
      myList[indexDoc] = doc.data();
      indexDoc++;
    });
    let myJson = JSON.stringify(myList);
    writeFile('data-products.json', myJson, (err) => {
      if(err) {
          console.log(err); // Do something to handle the error or just throw it
          throw new Error(err);
      }                                                                                                                                              
      console.log('Success!');
  });
                                                                                                                                                     
  })
  .catch((err) => {
    console.log('Error getting documents', err);
  });
      
db.collection('Profile').get()
  .then((snapshot) => {
    var indexDoc = 0;
    var myList = [];
    snapshot.forEach((doc) => {
      console.log(doc.id);
      console.log(doc);
      myList[indexDoc] = {userId:doc.id,...doc.data()};
      indexDoc++;

    });
    let myJson = JSON.stringify(myList);

    writeFile('data-profile.json', myJson, (err) => {
      if(err) {
          console.log(err); // Do something to handle the error or just throw it
          throw new Error(err);
      }                                                                                                                                              
      console.log('Success!');
  });
                                                                                                                                                     
  })
  .catch((err) => {
    console.log('Error getting documents', err);
  });


      
