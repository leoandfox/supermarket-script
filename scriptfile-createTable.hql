CREATE SCHEMA IF NOT EXISTS supermarket;

CREATE EXTERNAL TABLE IF NOT EXISTS supermarket.transaction_table

(`date` STRING,`listProducts` STRING,`userId` STRING,`totalPrice` DOUBLE)

ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'

STORED AS TEXTFILE LOCATION 'hdfs://sandbox-hdp.hortonworks.com:8020/tmp/data' TBLPROPERTIES("skip.header.line.count"="2");

CREATE TABLE IF NOT EXISTS supermarket.transaction_orc
(`date` STRING,`listProducts` STRING,`userId` STRING,`totalPrice` DOUBLE)
STORED AS ORC;
