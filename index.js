const admin = require('firebase-admin');
const fs = require('fs');
const readFile = require('fs').readFile;
const writeFile = require('fs').writeFile;
const { Parser } = require('json2csv');
const fields = ['date', 'listProducts', 'userId', 'totalPrice'];
const json2csvParser = new Parser({ fields });


var serviceAccount = require("./supermarket-c24f6-6c2cf21808e2.json");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://supermarket-c24f6.firebaseio.com"
});

let db = admin.firestore();
db.collection('Transaction').get()
  .then((snapshot) => {
    var indexDoc = 0;
    var myList  = [];
    snapshot.forEach((doc) => {
      myList[indexDoc] = doc.data();
      indexDoc++;
    });
    const csv = json2csvParser.parse(myList);
    writeFile('data-transaction.csv', csv, (err) => {
      if(err) {
          console.log(err); // Do something to handle the error or just throw it
          throw new Error(err);
      }
      console.log('Success!');
  });
    
  })
  .catch((err) => {
    console.log('Error getting documents', err);
  });
db.collection('HistoryPosition').get()
  .then((snapshot) => {
    var indexDoc = 0;
    var myList = [];
    snapshot.forEach((doc) => {
      myList[indexDoc] = doc.data();
      indexDoc++;
    });
    const csv = json2csvParser.parse(myList);
    writeFile('data-position.csv', csv, (err) => {
      if(err) {
          console.log(err); // Do something to handle the error or just throw it
          throw new Error(err);
      }                                                                                                                                              
      console.log('Success!');
  });
                                                                                                                                                     
  })
  .catch((err) => {
    console.log('Error getting documents', err);
  });
      
db.collection('Products').get()
  .then((snapshot) => {
    var indexDoc = 0;
    var myList = [];
    snapshot.forEach((doc) => {
      myList[indexDoc] = doc.data();
      indexDoc++;
    });
    const csv = json2csvParser.parse(myList);
    writeFile('data-products.csv', csv, (err) => {
      if(err) {
          console.log(err); // Do something to handle the error or just throw it
          throw new Error(err);
      }                                                                                                                                              
      console.log('Success!');
  });
                                                                                                                                                     
  })
  .catch((err) => {
    console.log('Error getting documents', err);
  });
      
db.collection('Profile').get()
  .then((snapshot) => {
    var indexDoc = 0;
    var myList = [];
    snapshot.forEach((doc) => {
      myList[indexDoc] = doc.data();
      indexDoc++;
    });
    const csv = json2csvParser.parse(myList);
    writeFile('data-profile.csv', csv, (err) => {
      if(err) {
          console.log(err); // Do something to handle the error or just throw it
          throw new Error(err);
      }                                                                                                                                              
      console.log('Success!');
  });
                                                                                                                                                     
  })
  .catch((err) => {
    console.log('Error getting documents', err);
  });
      
